# Pipelines rsync Deployment Example

Pipelines rsync deployment example project, deploy files to a server from [Bitbucket](https://bitbucket.org) using [Pipelines](https://bitbucket.org/product/features/pipelines). Perfect for PHP applications running on a Unix-like server!

## Setup

Enable pipelines in your repository by visiting the Bitbucket GUI `Settings -> Pipelines -> Settings -> Enable`.

### Server

In terms of your server side setup I recommend creating a dedicated _deploy_ user to transfer your application files to the server, this is detailed really well in [this article](https://mikeeverhart.net/2016/02/add-a-deploy-user-to-a-remote-linux-server/) by Mike Everhart. It outlines all the steps required from setting up the user to ensuring the file permissions are correct for the deploy directories.

If you're still facing file/directory issues I recommend checking out [this article](http://fideloper.com/user-group-permissions-chmod-apache) by fideloper which goes into detail on setting up user and group permissions for Apache.

### SSH Keys

Since we will be using SSH to deploy the code we can take advantage of _passwordless_ logins by using public key authentication! You can either use your own existing SSH keys or generate unique ones that are tied to Bitbucket - I suggest the latter along with the use of a dedicated deploy user.

Browser to `Settings -> Pipelines -> SSH Keys` in the Bitbucket GUI and follow the instructions [here](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html). Basically it involves two simple steps:

* Add SSH Key
* Add Known Hosts (ip or hostname)

### Required Environment Variables

Setup these variables via the Bitbucket GUI `Settings -> Pipelines -> Environment variables`.

* `DEPLOY_HOST` ip or hostname of the remote server.
* `DEPLOY_USER` username of the SSH user.
* `DEPLOY_PATH` the destination path on the remote server.

### Deployment Exclude List

By default all files not directly related to the running of the app are excluded from deployment (bitbucket-pipelines.yml, deployment-exclude-list.txt, README.md and the git directory). You can exclude additional files and or directories as required, also note [Globbing](https://stackoverflow.com/questions/30229465/what-is-file-globbing "Globbing") is supported! e.g. `/.*` will exclude all files in the _root_ of the project that begin with a dot.
